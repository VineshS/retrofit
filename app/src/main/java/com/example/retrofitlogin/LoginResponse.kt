package com.example.retrofitlogin

data class LoginResponse(val status:String?=null,
                         val message:String?=null,
                         val data:LoginData?=null)

data class Error(val status: String?,val message: String?)