package com.example.retrofitlogin

import com.google.gson.JsonObject
import retrofit2.Response

interface IRegistrationView:MvpView {

    fun onRegistrationSuccess(loginBase:RegistrationResponse)
    fun onRegistrationError(loginBase: Error)

    fun onCountrySuccess(loginBase: Response<JsonObject>)
    fun onCountryError(loginBase: Error)

    fun countrySpinnerSuccess(response: Response<JsonObject>)
    fun stateSpinnerSuccess(response: Response<JsonObject>)
}