package com.example.retrofitlogin

interface ILoginView:MvpView {

    fun onSuccess(loginBase:LoginResponse)

    fun onError(error: Error)
}