package com.example.retrofitlogin

data class StateData(
        val state_id: String,
        val state_name: String
)