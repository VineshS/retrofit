package com.example.retrofitlogin

data class RegistrationResponse(
        val data: RegistrationData,
        val message: String,
        val status: String
)

data class RegistrationError(val status: String?,val message: String?)