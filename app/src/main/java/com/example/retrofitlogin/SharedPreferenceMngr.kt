package com.example.retrofitlogin

import android.content.Context

class SharedPreferenceMngr private constructor(private val context: Context){

    val isLoggedIn: Boolean
    get() {
        val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        return sharedPreferences.getInt("user_id",-1)!=-1
    }

    val user:LoginData
    get() {
        val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        return LoginData(
            sharedPreferences.getInt("user_id",-1),
            sharedPreferences.getString("email_id",null),
            sharedPreferences.getInt("user_type",1)
        )
    }

    fun saveUser(user: LoginData) {
        val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.putInt("user_id",user.user_id)
        editor.putString("email_id",user.email_id)
        editor.putInt("user_id",user.user_type)

        editor.apply()

    }

    fun clear() {
        val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }


    companion object {
        private val  SHARED_PREF_NAME = "shared_pref"
        private var instance:SharedPreferenceMngr? = null

        fun getInstance(context: Context):SharedPreferenceMngr {
            if (instance == null){
                instance = SharedPreferenceMngr(context)
            }
            return  instance as SharedPreferenceMngr
        }
    }

}