package com.example.retrofitlogin

interface MvpView {

    fun showLoading()
    fun hideLoading()
    fun isNetworkConnected() : Boolean
    fun showMessage(message:String)

}
