package com.example.retrofitlogin

data class CountryResponse(
        val data: List<CountryData>,
        val message: String,
        val status: String
)
