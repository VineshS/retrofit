package com.example.retrofitlogin

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegistrationPresenter(var iRegistrationView: IRegistrationView, var context: Context):IRegistrationPresenter {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()

    private lateinit var countryList : Array<CountryData>
    private lateinit var stateList : Array<StateData>
    private lateinit var cityList : Array<CityData>
    private lateinit var cntryId :String
    private lateinit var stateId:String
    private lateinit var cityId:String

    override fun registrationApi(firstName: String, lastName: String, phone: String, email: String, password: String, cntryId: String, stateId: String, cityId: String, userType: Int) {

      if (iRegistrationView.isNetworkConnected()) {

          RetrofitObject.instance.regList(firstName, lastName, phone, email, password, cntryId, stateId, cityId, userType)
                  .enqueue(object : Callback<JsonObject> {
                      override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                          when {
                              response.code() == 400 -> {
                                  val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                  iRegistrationView.onRegistrationError(loginBase)
                                  //Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()


                              }
                              response.code() == 200 -> {
                                  val loginBase = gson.fromJson(response.body().toString(), RegistrationResponse::class.java)
                                  iRegistrationView.onRegistrationSuccess(loginBase)
                                  // Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()
//                             startActivity(Intent(applicationContext,MainActivity::class.java))


                              }
                              else -> {
                                  // Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
                              }

                          }
                      }

                      override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                      }

                  })

      }
        else{
            iRegistrationView.showMessage(context.resources.getString(R.string.no_internet))
      }


    }

    override fun countryApi() {
        RetrofitObject.instance.countryList()
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        when{
                            response.code() == 200 -> {

//                            val res = response
                              iRegistrationView.onCountrySuccess(response)
                            //CountrySpinnerLoad(response)
                            }

                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                iRegistrationView.onCountryError(res)
                            }

                        }

                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                     //   Toast.makeText(applicationContext, "Some error", Toast.LENGTH_SHORT).show()
                    }

                })
    }

    override fun itemCountrySpinner(cntryId: String) {

        RetrofitObject.instance.stateList(cntryId)
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        when{

                            response.code() == 200 -> {


                                iRegistrationView.countrySpinnerSuccess(response)
//                                StateSpinnerLoad(response)
                            }

                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            }


                        }
                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                       iRegistrationView.showMessage("Some Error")
                    }

                })
    }

    override fun itemStateSpinner(stateId: String) {
        RetrofitObject.instance.cityList(stateId)
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        when{

                            response.code() == 200 -> {
                            iRegistrationView.stateSpinnerSuccess(response)

                            //    CitySpinnerLoad(response)
                            }

                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            }


                        }

                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    }

                })
    }
}