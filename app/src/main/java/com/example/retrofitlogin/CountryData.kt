package com.example.retrofitlogin

data class CountryData(
        val country_id: String,
        val country_name: String
)
