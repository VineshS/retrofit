package com.example.retrofitlogin

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

open class BaseActivity : AppCompatActivity(),MvpView {

    override fun isNetworkConnected() : Boolean {
        val connectivityManager=this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo?.isConnectedOrConnecting == true
    }

    override fun showMessage(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        progressMainActivityprogressBar?.visibility = View.VISIBLE
    }
    override fun hideLoading() {
        progressMainActivityprogressBar?.visibility = View.GONE
    }

}