package com.example.retrofitlogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        logoutHomeActivityButton.setOnClickListener {
            startActivity(Intent(this ,MainActivity::class.java))
            SharedPreferenceMngr.getInstance(applicationContext).clear()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId) {

            R.id.AboutID -> {Toast.makeText(this,"About selected",Toast.LENGTH_SHORT).show()}
            R.id.SettingsID -> {Toast.makeText(this,"Settings selected",Toast.LENGTH_SHORT).show()}
            R.id.logoutID -> {
                val intent = Intent(applicationContext,MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                SharedPreferenceMngr.getInstance(applicationContext).clear()
            }
        }

        return super.onOptionsItemSelected(item)
    }



//    override fun onBackPressed() {
//        startActivity(Intent(this ,MainActivity::class.java))
//        finish()
//    }

//    override fun onStart() {
//        super.onStart()
//
//        if (!SharedPreferenceMngr.getInstance(this).isLoggedIn) {
//            val intent = Intent(applicationContext,MainActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//            startActivity(intent)
//        }
//
//    }
}