package com.example.retrofitlogin

data class CityResponse(
        val data: List<CityData>,
        val message: String,
        val status: String
)