package com.example.retrofitlogin

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(var iLoginView: ILoginView, var context: Context):ILoginPresenter {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()



    override fun callLoginApi(emailId: String, password: String, user_id: Int) {

        iLoginView.showLoading()
        if (iLoginView.isNetworkConnected()){

            RetrofitObject.instance.userLogin(emailId,password,user_id)
                        .enqueue(object : Callback<JsonObject> {
                            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                             iLoginView.hideLoading()

                                when {
                                    response.code() == 400 -> {
                                        val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                        iLoginView.onError(loginBase)
                                    }
                                    response.code() == 200 -> {
                                        val loginBase = gson.fromJson(response.body().toString(), LoginResponse::class.java)
                                        iLoginView.onSuccess(loginBase)
                                    }
                                    else -> {
                                        iLoginView.showMessage(context.resources.getString(R.string.something_went))
                                    }


                                }


                            }
                            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                           //    Toast.makeText(this@MainActivity,"Something is wrong",Toast.LENGTH_SHORT).show()
                            }

                        } )


                            }

        else{
            iLoginView.hideLoading()
            iLoginView.showMessage(context.resources.getString(R.string.no_internet))
        }

        }


    }
