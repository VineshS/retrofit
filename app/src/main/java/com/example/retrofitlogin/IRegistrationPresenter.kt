package com.example.retrofitlogin

interface IRegistrationPresenter {

  fun registrationApi(firstName:String,lastName:String,phone:String,email:String,password:String,cntryId:String,stateId:String,cityId:String
                      ,userType:Int)

  fun countryApi()

  fun itemCountrySpinner(cntryId:String)
  fun itemStateSpinner(stateId: String)

}