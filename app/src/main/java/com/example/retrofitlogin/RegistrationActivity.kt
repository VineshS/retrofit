package com.example.retrofitlogin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.view.get
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_registration.*
import retrofit2.Response

class RegistrationActivity : BaseActivity(),AdapterView.OnItemSelectedListener,IRegistrationView {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private lateinit var countryList : Array<CountryData>
    private lateinit var stateList : Array<StateData>
    private lateinit var cityList : Array<CityData>
    private lateinit var cntryId :String
    private lateinit var stateId:String
    private lateinit var cityId:String

    private val registrationPresenter = RegistrationPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

         // back button



        toolbar.title = ""
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        //

     //   countrySpinner.prompt = "Select Country"

        countrySpinner.onItemSelectedListener = this
        stateSpinner.onItemSelectedListener = this
        citySpinner.onItemSelectedListener = this


//        registrationPresenter.countryApi()


        registerButtonMainActivity.setOnClickListener {

            var firstName = firstnameMainActivity.text.toString()
            var lastName = lastnameMainActivity.text.toString()
            var phone = phoneMainActivity.text.toString()
            var email = emailMainActivity.text.toString()
            var password = passwordMainActivity.text.toString()
            var confirmPass = cnfpasswordMainActivity.text.toString()
            var userType =1


            if(isNetworkConnected()) {

                registrationPresenter.registrationApi(firstName, lastName, phone, email, password, cntryId, stateId, cityId, userType)
            }
            else{
                showMessage("Connect to Internet")
            }
        }

        registrationPresenter.countryApi()


    }





    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        when(parent?.id) {
            R.id.countrySpinner -> {

                cntryId = countryList.get(position).country_id
                Toast.makeText(applicationContext,cntryId,Toast.LENGTH_SHORT).show()
                registrationPresenter.itemCountrySpinner(cntryId)

            }

            R.id.stateSpinner -> {
                stateId = stateList.get(position).state_id
                //     Toast.makeText(this,stateId,Toast.LENGTH_SHORT).show()

                registrationPresenter.itemStateSpinner(stateId)


            }

            R.id.citySpinner -> {
                cityId = cityList.get(position).city_id
                //      Toast.makeText(this,cityId,Toast.LENGTH_SHORT).show()
            }

        }

    }



    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onRegistrationSuccess(loginBase: RegistrationResponse) {
        Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()
        startActivity(Intent(applicationContext,MainActivity::class.java))
    }

    override fun onRegistrationError(loginBase: Error) {
//        val loginBase = gson.fromJson(loginBase.errorBody()?.charStream(), Error::class.java)
        Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()
    }

    override fun onCountrySuccess(loginBase: Response<JsonObject>) {
//        CountrySpinnerLoad(loginBase)
        val res = gson.fromJson(loginBase.body().toString(), CountryResponse::class.java)
        countryList = res.data.toTypedArray()


        var country = arrayOfNulls<String>(countryList.size+1)


        country[0] = "Select Country"

        for (i in countryList.indices) {
                country[i+1] = countryList[i].country_name
        }

        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,country)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        countrySpinner.adapter = adapter

    }

    override fun onCountryError(loginBase: Error) {
        Toast.makeText(applicationContext, "Some error", Toast.LENGTH_SHORT).show()
    }

    override fun countrySpinnerSuccess(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), StateResponse::class.java)
        stateList = res.data.toTypedArray()
        var state = arrayOfNulls<String>(stateList.size+1)


        state[0]="Select State"
        for (i in stateList.indices) {

//            state[0]="Select State"
            state[i+1] = stateList[i].state_name
        }
        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,state)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        stateSpinner.adapter = adapter
    }

    override fun stateSpinnerSuccess(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), CityResponse::class.java)
        cityList = res.data.toTypedArray()
        var city = arrayOfNulls<String>(cityList.size+1)

        city[0]="Select city"

        for (i in cityList.indices) {

//            city[0]="Select city"
            city[i+1] = cityList[i].city_name

        }

        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,city)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        citySpinner.adapter = adapter
    }


}
