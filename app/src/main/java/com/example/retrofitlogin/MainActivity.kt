package com.example.retrofitlogin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity(),ILoginView {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()

    private val loginPresenter = LoginPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressMainActivityprogressBar.visibility = View.GONE

        LoginMainActivityButton.setOnClickListener {

            var email = usernameMainActivityEditText.text.toString().trim()
            var password = passwordMainActivityPasswordText.text.toString().trim()
            var userType: Int = 1

            loginPresenter.callLoginApi(email,password,userType)


            }

        signUpMainActivity.setOnClickListener {
            if (isNetworkConnected()) {
                startActivity(Intent(this, RegistrationActivity::class.java))
            }
            else
            {
                showMessage(applicationContext.resources.getString(R.string.no_internet))
            }
        }

        }

    override fun onSuccess(loginBase: LoginResponse) {
       showMessage(loginBase.message!!)

        if (loginBase.status=="success"){
            SharedPreferenceMngr.getInstance(applicationContext).saveUser(loginBase.data!!)
            val intent = Intent(applicationContext,HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
        else
        {
            showMessage("Something is Wrong")
        }
    }

    override fun onError(error: Error) {
        showMessage(error.message!!)
    }

}


